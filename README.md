# Force New Blog Templates #

This WordPress plugin adds a Gravity Forms setting to force a
[New Blog Templates](https://github.com/wpmudev/blogtemplates/) template.

## TODO ##

Fix Gravity Forms changed its settings API in version 3 so this needs fixing.

## Installing ##

This plugin can be installed using [composer](https://getcomposer.org/) by including the
repository. Below is an example `composer.json` file.

``` json
{
	"name": "example/example-wp-site",
	"description": "Example WordPress site",
	"repositories": [
		{
			"type": "vcs",
			"url": "git@bitbucket.org:lighthouseuk/wp-force-new-blog-templates.git",
			"only": [
				"lh/wp-force-new-blog-templates"
			]
		}
	],
	"require": {
		"lh/wp-force-new-blog-templates": "dev-main"
	}
}
```
