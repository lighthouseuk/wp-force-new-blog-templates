<?php
/**
 * Plugin Name: Force New Blog Templates
 * Plugin URI: https://bitbucket.org/lighthouseuk/wp-force-new-blog-templates
 * Description: Adds a Gravity form setting to force a New Blog Templates template.
 * Author: Mike Manger
 * Author URI: https://profiles.wordpress.org/mikemanger/
 * Version: 0.1
 * Text Domain: lh-fnbt
 * Requires Plugins: blogtemplates, gravityformsuserregistration
 * License: MIT License
 * License URI: http://opensource.org/licenses/MIT
 * Update URI: https://bitbucket.org/lighthouseuk/wp-force-new-blog-templates
 *
 * @package lh/fnbt
 */

if ( is_admin() && ! is_network_admin() ) {
	include_once 'settings.php';
}

/**
 * Returns the User Registration option value for a form
 *
 * @param string               $option Option key.
 * @param array<string, mixed> $form Form attributes.
 *
 * @return string|mixed|false option value or False if not found.
 */
function lh_fnbt_get_option( $option, $form ): mixed {
	// Make sure we have the User Registration plugin.
	if ( function_exists( 'gf_user_registration' ) ) {
		$form_id = rgar( $form, 'id' );
		$config  = gf_user_registration()->get_feeds( $form_id );
	} else {
		$config = false;
	}

	if ( empty( $config ) ) {
		return false;
	}

	$config            = current( $config );
	$multisite_options = rgar( $config['meta'], 'multisite_options' );
	if ( is_array( $multisite_options ) && isset( $multisite_options[ $option ] ) ) {
		return $multisite_options[ $option ];
	}
	return false;
}

/**
 * Checks the User Registration settings to see if the blog templates is active
 *
 * @param array<string, mixed> $form Form attributes.
 *
 * @return bool Whether the form is set to display blog templates selection
 */
function lh_fnbt_is_nbt_active( $form ): bool {

	$blog_templates = lh_fnbt_get_option( 'blog_templates', $form );
	if ( isset( $blog_templates ) && absint( $blog_templates ) ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Force blog template meta when signing up/creating a new blog.
 *
 * @param array<string, mixed> $meta Current meta.
 * @param array<string, mixed> $form Current form.
 *
 * @return array<string, mixed> Filtered meta array.
 */
function lh_fnbt_gform_user_registration_new_site_meta( $meta, $form ) {
	$force_blog_template = lh_fnbt_get_option( 'lh_fnbt_force_blog_template', $form );
	if ( isset( $force_blog_template ) && ! empty( $force_blog_template ) ) {
		$meta['blog_template'] = absint( $force_blog_template );
	}
	return $meta;
}
add_filter( 'gform_user_registration_new_site_meta', 'lh_fnbt_gform_user_registration_new_site_meta', 20, 2 );
add_filter( 'gform_user_registration_signup_meta', 'lh_fnbt_gform_user_registration_new_site_meta', 20, 2 );

/**
 * Removes NBT template selector from form
 *
 * @param string               $form_html Form HTML to filter.
 * @param array<string, mixed> $form Form attributes Form attributes.
 *
 * @return string HTML Form content
 */
function lh_fnbt_get_form_filter( $form_html, $form ) {
	if ( ! lh_fnbt_is_nbt_active( $form ) ) {
		return $form_html;
	}
	$form_id             = $form['id'];
	$force_blog_template = lh_fnbt_get_option( 'lh_fnbt_force_blog_template', $form );

	if ( isset( $force_blog_template ) && ! empty( $force_blog_template ) ) {
		ob_start();
		?>
			<script type="text/javascript">
				jQuery( document ).ready( function() {
					jQuery('#blog_template-selection').remove();
				} );
			</script>
		<?php
		$form_html .= ob_get_clean();
	}
	return $form_html;
}
add_filter( 'gform_get_form_filter', 'lh_fnbt_get_form_filter', 15, 2 );
