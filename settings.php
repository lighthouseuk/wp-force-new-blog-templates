<?php
/**
 * Hook into Gravity Forms admin settings.
 *
 * @package lh/fnbt
 */

/**
 * Display a new option for forcing a New Blog Templates
 * template in User registration Form Settings Page
 *
 * @TODO Update to use gform_userregistration_feed_settings_fields
 *
 * @param array<string, mixed> $config Current GForm attributes.
 */
function lh_fnbt_gform_user_registration_add_option_section( $config ): void {

	$nbt_settings      = nbt_get_settings();
	$templates         = $nbt_settings['templates'];
	$multisite_options = rgar( $config['meta'], 'multisite_options' );
	$current           = rgar( $multisite_options, 'lh_fnbt_force_blog_template' );
	?>
		<div class="margin_vertical_10">
			<label class="left_header" for="lh_fnbt_force_blog_template"><?php esc_html_e( 'Force template', 'lh-fnbt' ); ?></label>
			<select id="lh_fnbt_force_blog_template" name="lh_fnbt_force_blog_template" />
				<option value=""></option>
				<?php foreach ( $templates as $key => $template ) : ?>
					<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $current, $key ); ?>><?php echo esc_html( $template['name'] ); ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	<?php
}
add_action( 'gform_user_registration_add_option_section', 'lh_fnbt_gform_user_registration_add_option_section', 16, 1 );

/**
 * Save the option for New Blog Templates
 * in User Registration Form Settings Page
 *
 * @param array<string, mixed> $config Current Form attributes.
 * @return array<string, mixed>
 */
function lh_fnbt_gform_pre_form_settings_save( $config ): array {
	$config['meta']['multisite_options']['lh_fnbt_force_blog_template'] = RGForms::post( 'lh_fnbt_force_blog_template' );
	return $config;
}
add_filter( 'gform_user_registration_save_config', 'lh_fnbt_gform_pre_form_settings_save' );
